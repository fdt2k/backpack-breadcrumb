<?php

namespace KDA\Backpack\Breadcrumb;

use Request;


trait breadcrumbOperation{
	
    protected function setupBreadcrumbDefaults()
    {
        $this->bcmanager = app()->make(BCManager::class);

        $this->data['breadcrumbs'] = [
            
        ];
        foreach($this->bcmanager->retrieve() as $key){
           
            $item = $this->bcmanager->retrieveByKey($key);
            $this->data['breadcrumbs'] [$item['label']]=  $item['route'];
        }   
    }

    protected function storeBC($label){
        $this->bcmanager->store($label,Request::url());
        $this->data['breadcrumbs'] = [
            
        ];
        foreach($this->bcmanager->retrieve() as $key){
           
            $item = $this->bcmanager->retrieveByKey($key);
            $this->data['breadcrumbs'] [$item['label']]=  $item['route'];
        }   
    }
}
