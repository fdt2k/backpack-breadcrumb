<?php

namespace KDA\Backpack\Breadcrumb;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{



    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


    public function register()
    {
        parent::register();

        $this->app->singleton(BCManager::class, function ($app) {
            return new BCManager();
        });
    }
}
