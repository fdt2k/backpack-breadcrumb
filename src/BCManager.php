<?php

namespace KDA\Backpack\Breadcrumb;



class BCManager
{
	public $current;
	protected $history;
	protected $history_ordered;

	public function __construct()
	{
		//$this->action = $action;

		$this->history = session('history') ?? [];
		$this->history_ordered = session('history_ordered') ?? [];
	}

	public function reset()
	{
		$this->history = array();
		$this->history_ordered = array();
		session('history', []);
		session('history_ordered', []);
		return $this;
	}

	public function retrieve()
	{
		return session('history_ordered') ?? [];
	}

	public function retrieveByKey($key)
	{
		$history =  session('history');

		return $history[$key];
	}
	public function store($label, $route, $get = array(), $post = array())
	{
		$this->storeHistory(ucFirst($label), $route, $get, $post);
	}

	public function storeHistory($desc, $route, $qs, $post)
	{


		$item = [
			'label' => $desc,
			'route' => $route,
			'qs' => $qs,
			'post' => $post
		];
		$key = md5(serialize($item));

		$datas = $item;

		if (!@in_array($key, $this->history_ordered)) {
			$this->history_ordered[] = $key;
			$this->history[$key] = $datas;
		} else { // on efface l'historique jusqu'a revenir a ce point précis (l'actuel)
			// on met a jour les parametres
			$this->history[$key] = $datas;

			$bClear = false;
			foreach ($this->history_ordered as  $datas) {
				if (!$bClear) { // on parcours la liste tant qu'on a pas trouvé le point actuel.
					$tmp[] = $datas; // on stocke la clé pour plus tard

					if ($key == $datas) { // on a atteint le point actuel
						$this->history_ordered = $tmp; //on met a jour la liste triée
						$bClear = true;
					}
				} else {
					unset($this->history[$datas]);
				}
			}
		}

		session(['history' => $this->history]);
		session(['history_ordered' => $this->history_ordered]);
	}

	function getStates()
	{
		$tmp = array();
		$current_bundle = Env::getRoute()->bundle;
		if (is_array($this->history_ordered[$current_bundle])) {
			foreach ($this->history_ordered[$current_bundle] as $key) {

				$tmp[] = array('key' => $key, 'module' => $this->history[$current_bundle][$key]->getModule(), 'string' => $this->history[$current_bundle][$key]->getLabel());
			}
		}
		return $tmp;
	}


	public function getStoredState($key)
	{
		//var_dump($this->history[$current_bundle][$key]);
		$current_bundle = Env::getRoute()->bundle;
		return $this->history[$current_bundle][$key];
	}

	public function restoreState($key)
	{

		$state = $this->getStoredState($key);
		if ($state) {
			Env::$post->setDatas($state->getPost());
			Env::$get->setDatas($state->getQuerystring());
			Env::getRoute()->action = $state->getAction();


			// delete everything after the current point
			$clear = false;
			$current_bundle = Env::getRoute()->bundle;
			$array = $this->history_ordered[$current_bundle];
			foreach ($array as $k => $datas) {
				if ($key == $datas) {

					$clear = true;
				}

				if ($clear) {
					unset($array[$k]);
				}
			}
			$this->history_ordered[$current_bundle] = $array;
			return $state;
		}
		return false;
	}

	public function getStateError(array $formDatas = array())
	{
		//var_dump($formDatas);
		$datas = $this->history_ordered[$current_bundle];
		$key = $datas[sizeof($datas) - 1];
		if (!empty($key)) {
			if (sizeof($formDatas) > 0) {
				//injecting params

				$params = unserialize($this->history[$current_bundle][$key]['params']);

				foreach ($formDatas as $k => $value) {
					if (!isset($params[$k])) {
						$params[$k] = $value;
					}
				}

				$params = serialize($params);

				$this->history[$current_bundle][$key]['params'] = $params;
			}
			return $key;
		}
		return false;
	}

	public function getLastState(array $formDatas = array())
	{
		//var_dump($formDatas);
		$current_bundle = Env::getRoute()->bundle;
		$datas = $this->history_ordered[$current_bundle];
		$key = $datas[sizeof($datas) - 1];

		#	var_dump( $this->history_ordered);
		if (!empty($key)) {
			if (sizeof($formDatas) > 0) {
				//injecting params

				$params = unserialize($this->history[$current_bundle][$key]['params']);

				foreach ($formDatas as $k => $value) {
					if (!isset($params[$k])) {
						$params[$k] = $value;
					}
				}

				$params = serialize($params);

				$this->history[$current_bundle][$key]['params'] = $params;
			}
			return $key;
		}
		return false;
	}

	public function getStateSuccess()
	{
		$datas = $this->history_ordered[$current_bundle];
		$key = $datas[sizeof($datas) - 2];
		if (!empty($key)) {
			return $key;
		}
		return false;
	}

	public function setError($key, $error)
	{
		$this->history[$current_bundle][$key]['error'] = $error;
	}
}
